﻿using System;
using DryIoc;
using DryIoc.Microsoft.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;

namespace DryIoCMqttServer.Configuration
{
    public class IoCBootstrapper
    {
        public static IServiceProvider Init(IServiceCollection services)
        {
            return new Container().WithDependencyInjectionAdapter(services,
                    throwIfUnresolved: type => type.Name.EndsWith("Controller"))
                .ConfigureServiceProvider<CompositionRoot>();
        }
    }
}
