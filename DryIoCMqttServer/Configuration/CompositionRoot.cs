﻿using DryIoc;
using DryIoCMqttServer.Service;

namespace DryIoCMqttServer.Configuration
{
    public class CompositionRoot
    {
        public CompositionRoot(IRegistrator r)
        {
            r.Register<IUserService, UserService>(Reuse.InCurrentScope);
        }
    }
}
