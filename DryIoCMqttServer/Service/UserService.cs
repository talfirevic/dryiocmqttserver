﻿using System;

namespace DryIoCMqttServer.Service
{
    public interface IUserService
    {
        bool Test();
    }

    public class UserService : IUserService
    {
        public bool Test()
        {
            var rand = new Random(1337);
            return rand.Next(2) == 0;
        }
    }
}
